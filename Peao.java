public class Peao extends Peca{
	
	public Peao(int x, int y, int t){
		super(x,y,t);
		setTime(t);
	}
	
	public boolean mover(int x, int y, Jogo j){
	
		int diferenca = Util.modulo(getPos()[1] - y);
		
		if(getPos()[0] != x)
			return false;
			
		if (diferenca <= 2){
			if (((y != 2) && (y != 6)) && (diferenca == 2))
				return false;
		
			setPos(x,y);
			return true;
		}
		
		return false;
	}
	
	//Peao sobrescreve metodo pai, pois seu ataque difere de seu movimento
	public boolean atacar(Peca p, int x, int y, Jogo j){
		if((Util.modulo(y-getPos()[1]) > 1) || !(Util.diagonal(this, x, y, j)))
			return false;
		
		setPos(x,y);
		j.setTabuleiro(x,y, this);
		p.morrer(j);
		
		return true;
	}
}