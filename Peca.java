public abstract class Peca{
	
	private int pos[];
	private int time;
	
	public Peca(int x, int y, int t){
		this.pos = new int[2];
		setPos(x,y);
		this.setTime(t);
	}
	
	public int[] getPos(){
		return this.pos;
	}
	
	public void setPos(int x, int y){
		this.pos[0] = x;
		this.pos[1] = y;
	}
	
	public int getTime(){
		return this.time;
	}
	
	public void setTime(int t){
		this.time = t;
	}
	
	protected void morrer(Jogo j){
		//Envia as coordenadas para eliminar a peca[null]
		j.setTabuleiro(getPos()[0], getPos()[1], null);
	}
	
	public abstract boolean mover(int x, int y, Jogo j);
	
	//Este metodo � comum para todas as pecas ex
	public boolean atacar(Peca p, int x, int y, Jogo j){
		if (!mover(x, y, j))
			return false;
		
		j.setTabuleiro(x,y, this);
		p.morrer(j);
		
		return true;
	}
	
	public static void qualEh(Peca p){
	
		if(p instanceof  Peao)
			System.out.println("Peao");
		
		else if(p instanceof Cavalo)
			System.out.println("Cavalo");
			
		else if(p instanceof Torre)
			System.out.println("Torre");
			
		else if(p instanceof Bispo)
			System.out.println("Bispo");
			
		else if(p instanceof Rainha)
			System.out.println("Rainha");
			
		else if(p instanceof Rei)
			System.out.println("Rei");
				
		else 	
			System.out.println("Vazio");
	}
}
