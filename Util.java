 import java.util.Scanner;
 
 public class Util{
	
	public static int modulo(int numero){
	
		if (numero < 0)
			numero = numero * -1;
		return numero;
	}
	
	//verifica se o movimento com o diagonal eh valido
	public static boolean diagonal(Peca p, int x, int y, Jogo game){
		
		x = x - p.getPos()[0];
		y = y - p.getPos()[1];
		
		//Verifica se o movimento esta correspondente ao movimento diagonal
		if(Util.modulo(x) != Util.modulo(y))
			return false;
		
		//Verifica se ha uma peca no caminho
		int i = 1;
		int j = 1;
		
		i = x < 0 ? i*-1 : i;
		j = y < 0 ? j*-1 : j;

		//Verifica no intervalo origem-destino se ha alguma peca
		while(Util.modulo(i) < Util.modulo(x)){
		
			if(game.getTabuleiro()[p.getPos()[0]+i][p.getPos()[1]+j].getPeca() != null)
				return false;	//Encontrou uma peca no caminho, retorna true
			
			//Dependendo, incrementa ou decrementa
			i = i > 0 ? i+1 : i-1;
			j = j > 0 ? j+1 : j-1;	
		}
		
		//Nenhum empecilho, movimento valido
		return true;
	}
	
	//verifica se o movimento na vertical ou horizontal eh valido
	public static boolean cruzado(Peca p, int x, int y, Jogo game){
	
		//Verifica se esta na mesma linha ou na mesma coluna, e nao nos dois ao mesmo tempo (XOR)
		if(!((p.getPos()[0] == x) ^ (p.getPos()[1] == y)))
			return false;
			
		int i, tam, pos;
		i = 1;
		
		//Procura por alguma peca no caminho de destino
		if(p.getPos()[0] != x){		//Verifica se esta se movimentando na horizontal
		
			tam = Util.modulo(p.getPos()[0] - x);	//obtem a diferenca de casas da posicao
			
			while(i < tam){			//Verifica da proxima casa ate o destino, se ha uma peca no caminho
				pos = i;			//Variavel que deslocara as casas ate o destino
				
				if(p.getPos()[0] < x)	//Caso esteja regredindo, pos deve ser negativo para voltar casas
					pos = -pos;
				
				if(game.getTabuleiro()[p.getPos()[0] + pos][p.getPos()[1]].getPeca() != null)	//A posicao atual somada ao deslocamento
					return false;	//Encontrou uma peca no caminho: retorna false
				
				i++;				//Aumenta o deslocamento
			}
		}
		else{						//Verifica se esta se movimentando na vertical
		
			tam = Util.modulo(p.getPos()[1] - y);
			
			while(i < tam){			//Verifica da proxima casa ate o destino, se ha uma peca no caminho
				pos = i;			//Variavel que deslocara as casas ate o destino
			
				if(p.getPos()[0] < x)	//Caso esteja regredindo, pos deve ser negativo para voltar casas
					pos = -pos;
				
				if(game.getTabuleiro()[p.getPos()[0]][p.getPos()[1] + pos].getPeca() != null)	//A posicao atual somada ao deslocamento
					return false;	//Encontrou uma peca no caminho: retorna false
				
				i++;				//Aumenta o deslocamento
			}
		}
		
		//Nenhum empecilho, movimento valido
		return true;
	}
	
	//Le e transforma do sistema classico de posicionamento do xadrez para o sistema do jogo. Ex: A,1 -> 0,0
	public static int[] sistemaPos(){
		int[] pos = new int[2];
		char c;
		int p;
		boolean erro;
		String entrada;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Entre com a posi��o: [A-H][1-8]");
		entrada = s.next();
		entrada = entrada.toUpperCase();	//Converte para maiusculas
		
		//Obtem letra
		c = entrada.charAt(0);
		//Obtem numero entre 1 e 8
		p = entrada.charAt(1)-48;
		
		//Validacao de entrada
		while (c < 'A' || c > 'H' || p < 1 || p > 8)
		{
			System.out.println("Valor invalido. Limites: A-H e 1-8. Entre novamente com os valores: [A-H][1-8] Ex: A8");
			entrada = s.next();
			entrada = entrada.toUpperCase();
		
			c = entrada.charAt(0);
			p = entrada.charAt(1)-48;
		}
		
		//Tranforma a letra em posicao cartesiana
		pos[0] = (int)c-65;
		pos[1] = 8-p;
		
		return pos;
	}
	
	//Recebido o rei, verifica se esta em xeque
	public static boolean verificaAtaque(Jogo game, Peca r, boolean direcao){
		Peca p;
		int i = 0;
		
		while(i < 8){
			//Verifica xeque na horizontal
			if(direcao)
				p = game.getTabuleiro()[i] [r.getPos()[1]].getPeca();
			//Verifica xeque na vertical
			else
				p = game.getTabuleiro()[r.getPos()[0]] [i].getPeca();
			
			if (p != null)	//Se ha uma peca
				if(p.getTime() != r.getTime()){	//se nao eh do time
					if (p instanceof Torre || p instanceof Rainha)	//Se eh amea�adora
						return true;
				}else
					return false;	//A primeira peca encontrada foi do proprio time
			
			//Verifica xeque na diagonal esquerda
			if(direcao)
				p = game.getTabuleiro()[i][i].getPeca();
			//Verifica xeque na diagonal direita
			else
				p = game.getTabuleiro()[7-i] [7-i].getPeca();
			
			if (p != null)	//Se ha uma peca
				if(p.getTime() != r.getTime()){	//se nao eh do time
					if (p instanceof Bispo || p instanceof Rainha)	//Se eh amea�adora
						return true;
				}else
					return false;	//A primeira peca encontrada foi do proprio time
		}
		
		//Verifica o ataque do lado oposto; Soh eh executada uma vez, vasculhando o lado oposto e um cheque do cavalo
		if(direcao)
			//Caso ainda nao verificou o lado inverso, faz isto e checa se o rei esta sob ataque do cavalo
			return verificaAtaque(game, r, false) || verificaCavaloAtaque(game,r);	//Retorna o xeque caso algum destes ameaca o rei
		
		return false;
	}
	
	
	//Verifica se o rei esta sobre ataque do cavalo
	public static boolean verificaCavaloAtaque(Jogo game, Peca r){
	
		int i, j;
		int p1, p2;
		Peca p;
		
		//Variaveis para as possiveis posicoes de um cavalo
		int x = 2;
		int y = -1;
		
		//Apartir da posicao do rei, verifica todas as casas onde o cavalo poderia ser hostil;
		//Para isso, gera as oito combina�oes e testa. Ex: Rei em 0,0: Gera: -2,1 1,2 2,-1, ... etc;
		for (i=0;i<4;i++){
			
			//Alterna os valores de x e y. Caso seja 1 -> 2; Caso seja 2 -> 1
			x = 2/x;
			y = 2/y;
			
			//Alterna as magnitudes, em cada caso
			if(Util.modulo(x) == 2)
				x = x * -1;
			if(Util.modulo(y) == 2)
				y = y * -1;
				
			for(j=0;j<2;j++){
				
				//Obtenho a possivel posicao do cavalo
				p1 = r.getPos()[0] + x;
				p2 = r.getPos()[1] + y;
				
				//Verifico se esta nos limites do tabuleiro
				if((p1 < 8 && p1 >= 0) && (p2 < 8 && p2 >= 0)){
					
					//Obtenho a peca nesta posicao
					p = game.getTabuleiro()[p1][p2].getPeca();
					
					//Se eh um cavalo
					if (p instanceof Cavalo)
						//Se eh do adversario
						if(p.getTime() != r.getTime())
							return true;	//Hostil, esta em xeque
					
					//Mais uma vez, alterna a magnitude dos sinais
					if(Util.modulo(x) == 1)
						x = x * -1;
					else
						y = y * -1;
				}
			}
		}
		
		//A ordem de verificao: (1,2) (-1,2) (-2,1) (-2,-1) (-1,-2) (1,-2) (2,-1) (2,-1) (2,1)
		
		//Todas as possibilidades verificadas, o rei nao esta sob a