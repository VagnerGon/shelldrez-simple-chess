public class Espaco{

	private Peca p;
	
	public Espaco(){
		this.p = null;
	}
		
	public Peca getPeca(){
		return this.p;
	}
	
	public void setEspaco(Peca _p){
		this.p = _p;
	}
	
	public Peca getEspaco(){
		return this.p;
	}
}
