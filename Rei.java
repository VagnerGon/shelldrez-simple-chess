public class Rei extends Peca{
	
	public Rei(int x, int y, int t){
		super(x,y,t);
		setTime(t);
	}
	
	public boolean mover(int x, int y, Jogo j){
		int diferencax = Util.modulo(getPos()[0] - x);
		int diferencay = Util.modulo(getPos()[1] - y);
		
		if((diferencax > 1) || (diferencay > 1))
			return false;
		
		setPos(x,y);
		return true;
		
		//Rei nao pode mover onde pode morrer
	}
	
	//Metodo que verifica se o rei esta em xeque
	public boolean xeque(Jogo j){
		int i;
		
		if(Util.verificaAtaque(j, this, true))
			return true;
		
		return false;
	}
	
	//Testa posicoes possiveis para averiguar xeque mate
	private boolean mate(Jogo game){
		int i, j;
		int x = getPos()[0];	//Obtem localizacao atual
		int y = getPos()[1];
		
		//Estes for encadeados vao geras as combinacoes de movimentos do rei
		for(i=-1;i<=1;i++)
			for(j=-1;j<=1;j++)
				//Verifica s esta dentro dos limites do tabuleiro
				if( (x+i >= 0) && (x+i < 8) && (y+j >= 0) && (y+j < 8) )
					//Descarta a posicao propria do rei
					if(i != 0 && j != 0)
						//Verifica se ha uma peca nesta posicao
						if(game.getTabuleiro()[x+i][y+j].getPeca() == null){
							//Cria um novo rei para envia-lo nesta posicao
							Rei r = new Rei(x+i,y+j,getTime());
							//Caso nao esteja sobre ataque, nao eh xeque mate
							if(!Util.verificaAtaque(game,r,true))
								return false;
						}
		//Nenhuma das alternativas tiveram escape, xeque mate
		return true;
	}
	
	/*public static void main(String args[]){
		Rei r = new Rei(5,5,1);
		if(!r.mate(game))
			System.out.println("Nao esta em cheque");
	}*/
}