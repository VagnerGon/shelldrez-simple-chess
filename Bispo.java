public class Bispo extends Peca{
	
	public Bispo(int x, int y, int t){
		super(x,y,t);
		setTime(t);
	}
	
	//Retorna TRUE casa moveu-se com sucesso
	public boolean mover(int x, int y, Jogo j){ 
		
		if ( ! Util.diagonal(this,x,y,j))
			return false;
			
		setPos(x,y);
		return true;
	}

}