public class Torre extends Peca{
	
	public Torre(int x, int y, int t){
		super(x,y,t);
		setTime(t);
	}
	
	public boolean mover(int x, int y, Jogo j){
	
		//Retorna false se o movimento eh invalido
		if(! Util.cruzado(this, x, y, j))
			return false;
		
		setPos(x,y);
		return true;
	}

}