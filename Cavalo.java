public class Cavalo extends Peca{
	
	public Cavalo(int x, int y, int t){
		super(x,y,t);
		setTime(t);
	}
	
	public boolean mover(int x, int y, Jogo j){
		
		int diferencax = Util.modulo(x - getPos()[0]);
		int diferencay = Util.modulo(y - getPos()[1]);
		
		if (!((diferencax == 2) && (diferencay == 1)))
			if (!((diferencax == 1) && (diferencay == 2)))
				return false;
		
		setPos(x,y);
		return true;
	}
}