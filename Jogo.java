import java.util.ArrayList;

public class Jogo{
	private Espaco tabuleiro[][];
	private ArrayList<Peca> l1;
	private ArrayList<Peca> l2;
	
	//Inicializador do jogo.
	public Jogo(){
		int i,j;
		
		//Zera todos os epacos do tabuleiro
		this.tabuleiro = new Espaco[8][8];
		for (i=0;i<8;i++)
			for(j=0;j<8;j++)
				this.tabuleiro[i][j] = null;
	}
	
	public void setTabuleiro(int x, int y, Peca p){
		this.tabuleiro[x][y].setEspaco(p);
	}
	
	public Espaco[][] getTabuleiro(){
		return this.tabuleiro;
	}

	//Remove peca do tabuleiro e da lista do jogador atacado
	public void matar(Peca p, int jogador){
		
		int x, y;
		
		//Obtem as posicoes da peca
		x = p.getPos()[0];
		y = p.getPos()[1];
		
		//remove do tabuleiro
		this.tabuleiro[x][y] = null;
		
		//True: Jogador1 | False: Jogador2
		//	[sendo atacado]
	}
	
	private void criaTabuleiro(){
		int i, j;
		this.tabuleiro = new Espaco[8][8];
		for(i=0;i<8;i++)
			for(j=0;j<8;j++)
				this.tabuleiro[i][j] = new Espaco();
	}
	
	//Preeche as listas com as pecas
	private void preencheTabuleiro(int jogador){
		int i, pos;
		
		//Verifica qual das posicoes ira adicionar
		if(jogador == 1)
			pos = 6;
		else
			pos = 1;
		
		//Adiciona os peoas na posicao relativa
		for(i=0;i<8;i++)
			this.tabuleiro[i][pos].setEspaco(new Peao(i,pos,jogador)); 	//Adiciona peoes
		
		//Dependendo do jogador, tem que ir para fileira de cima, ou para fileira de baixo
		if(pos == 1)
			pos--;
		else
			pos++;
		
		this.tabuleiro[0][pos].setEspaco(new Torre(0,pos,jogador)); 	//Adiciona Torre
		this.tabuleiro[1][pos].setEspaco(new Cavalo(1,pos,jogador)); 	//Adiciona Cavalo		
		this.tabuleiro[2][pos].setEspaco(new Bispo(2,pos,jogador)); 	//Adiciona Bispo
		this.tabuleiro[3][pos].setEspaco(new Rainha(3,pos,jogador)); 	//Adiciona Rainha
		this.tabuleiro[4][pos].setEspaco(new Rei(4,pos,jogador)); 		//Adiciona Rei
		this.tabuleiro[5][pos].setEspaco(new Bispo(5,pos,jogador)); 	//Adiciona Bispo
		this.tabuleiro[6][pos].setEspaco(new Cavalo(6,pos,jogador)); 	//Adiciona Cavalo
		this.tabuleiro[7][pos].setEspaco(new Torre(7,pos,jogador)); 	//Adiciona Torre

	}
	
	//O que sera feito em cada turno
	private void jogar(int jogador){
		
		//Pos1 -> posicao da peca manipulada
		//Pos2 -> posicao de destino
		int pos1[], pos2[] = new int[2];
		Peca p1, p2;
		
		System.out.println("Jogador " + jogador + ", sua vez:");
		System.out.print("Selecione a pe�a: ");
		
		//Imprime qual a peca selecionada
		do{
			do{
				pos1 = Util.sistemaPos();		//Obtem a coordenada
				p1 = this.tabuleiro[pos1[0]][pos1[1]].getEspaco();	//Obtem o espaco
				Peca.qualEh(p1);				//Mostra na tela o que achou
			}while (p1 == null);
			//Mantem em loop caso esteja selecionando um espaco vazio
			if(p1.getTime() != jogador)
				System.out.println("Esta peca nao pertence a voce");
		}while(p1.getTime() != jogador);
		//Mantem em loop caso o jogador selecione uma peca que nao eh dele
		
		boolean pode;
		
		//Verifica se o movimento eh valido
		do{
			do{
				System.out.print("Selecione o destino: ");
				pos2 = Util.sistemaPos();
				pode = p1.mover(pos2[0],pos2[1], this);
				if(!pode)
					System.out.print("Este movimento eh invalido para a peca selecionada. ");
			}while(!pode);
			
			//Obtem o que ha na posicao de destino
			p2 = this.tabuleiro[pos2[0]][pos2[1]].getEspaco();
			
			//Caso o destino seja uma peca propria
			pode = false;
			if(p2 != null)
				pode = p2.getTime() == p1.getTime();
			if(pode)
				System.out.println("Esta peca pertence ao seu jogo");
		}while(pode);
		
		if(p2 != null){
			//Achou uma peca
			
			System.out.print("Peca Capturada: ");
			Peca.qualEh(p2);		//Informa qual peca achou
			
			//Efetua o ataque na peca destino com a peca em uso
			this.tabuleiro[pos1[0]][pos1[1]].getEspaco().atacar(p2,pos1[0],pos1[1],this);
			
			//Remove a peca da lista do jogador adversario
			matar(p2,jogador);
		}
		else{
			//Posicao vazia, move a peca	
			p1.setPos(pos2[0],pos2[1]);
			this.tabuleiro[pos2[0]][pos2[1]].setEspaco(p1);
			this.tabuleiro[pos1[0]][pos1[1]].setEspaco(null);	//Zera pocisao onde a peca estava
			
			System.out.println("Movido");
		}
	}
	
	public static void main(String[] args){
		
		Jogo j = new Jogo();
		int i = 1;
		System.out.println("Xadrez Beta");
		j.criaTabuleiro();
		j.preencheTabuleiro(1);		//Adiciona as pecas para o jogador 1
		j.preencheTabuleiro(2);		//Adiciona as pecas para o jogador 2
		
		//Jogo principal
		while (true){
			
			j.jogar(i);
			
			//Alterna entre os jogadores
			i = 2/i;
		}
	}
}
