public class Rainha extends Peca{
	
	public Rainha(int x, int y, int t){
		super(x,y,t);
		setTime(t);
	}
	
	public boolean mover(int x, int y, Jogo j){
	
		//Valida se ele nao se movimenta de forma cruzada ou em diagonal E NAO OS DOIS AO MESMO TEMPO (OU exclusivo)
		if (!(Util.diagonal(this,x,y,j) ^ Util.cruzado(this,x,y,j)))
			return false;	//Movimento invalido
		
		setPos(x,y);
		return true;
	}

}